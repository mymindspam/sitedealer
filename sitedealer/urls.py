from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^deals/', include('deals.urls', namespace='deals')),
    url(r'^admin/', include(admin.site.urls)),
    url('', include('social.apps.django_app.urls', namespace='social')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'loginoauth.views.home', name='home'),
    url(r'^members/', 'loginoauth.views.members', name='members'),
    url(r'^logout/$', 'loginoauth.views.logout', name='logout'),
)
if settings.DEBUG:
    import debug_toolbar
    urlpatterns += patterns('', url(r'^__debug__/', include(debug_toolbar.urls)), )
