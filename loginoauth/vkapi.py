import requests
import time

VK_API = 'https://api.vk.com/method/'
class VkApiExeption(Exception):
    def __init__(self, response):
        error = response['error']
        self.error_code = error['error_code']
        self.error_msg = error['error_msg']
        self.request_params = error['request_params']
        message = 'error_code: %s error_msg: %s request_params: %s' \
                  % (self.error_code, self.error_msg, self.request_params)
        Exception.__init__(self, message)


class VkApi():
    def __init__(self, acess_token, version=5.25):
        self.acess_token = acess_token
        self.version = version

    def call(self, method, **kwargs):
        kwargs['access_token'] = self.acess_token
        kwargs['v'] = self.version
        response = requests.get(VK_API+method, params=kwargs).json()
        if 'error' in response:
            vk_api_exeption = VkApiExeption(response)
            if vk_api_exeption.error_code == 6:
                time.sleep(10)
                self.call(method, **kwargs)
            else:
                raise vk_api_exeption
        else:
            return response['response']

    def serch_and_get_groups(self, query, type, country, city, offset, page_items_amount):
        groups = self.call('groups.search',
                            q=query, type=type, country_id=country, city_id=city, offset=offset, count=page_items_amount)
        total = groups['count']
        groups = groups['items']

        ids = [group['id'] for group in groups]
        if not len(ids) == 0:
            groups = self.call('groups.getById',
                                group_ids=str(ids)[1:-1], fields='description,site,contacts')
        return total, groups

    def get_full_group_info(self, group_id):
        group = self.call('groups.getById', group_id=group_id, fields='description,site,contacts')[0]
        group['albums'] = self.call('photos.getAlbums', owner_id='-' + str(group['id']), need_system=0)
        user_ids = [contact['user_id'] for contact in group['contacts']]
        user_desc = {contact['user_id']: contact['desc'] for contact in group['contacts'] if 'desc' in contact}
        group['contacts'] = self.call('users.get', user_ids=str(user_ids)[1:-1], fields='photo_50,can_write_private_message')
        for contact in group['contacts']:
            if contact['id'] in user_desc:
                contact['desc'] = user_desc[contact['id']]
            else:
                contact['desc'] = ''
        return group

    def get_albums_and_photos(self, owner_id, album_ids, amount):
        albums = self.call('photos.getAlbums', owner_id='-'+owner_id, album_ids=album_ids)['items']
        for album in albums:
            album['photos'] = self.call('photos.get', owner_id='-'+owner_id, album_id=album['id'])['items']
        return albums