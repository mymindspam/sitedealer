from django.shortcuts import render, redirect
from django.contrib.auth import logout as auth_logout
from django.http import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt


def home(request):
    context = {}
    template = 'home.html'
    return render(request, template, context)


def members(request):
    context = {}
    template = 'members.html'
    request.session['vk-oauth2_access_token'] = request.user.social_auth.get(provider='vk-oauth2').extra_data['access_token']
    return render(request, template, context)


def logout(request):
    auth_logout(request)
    return redirect('/')
