import time
import os
import zipfile

import requests
from wordpress_xmlrpc import Client

from sitebuilder.sitedealer.snapstart import adapters


def unzip(origin, dest):
    with zipfile.ZipFile(origin, "r") as wp_zip:
        wp_zip.extractall(dest)


def unzip_theme(origin, name, dest):
    theme_path = os.path.join(origin, name, name+'.zip')
    with zipfile.ZipFile(theme_path, "r") as theme_zip:
        theme_zip.extractall(dest)


def create_wp_config(site_path, db_name, db_user_name, db_user_password, db_host='localhost'):
    wp_config_sample = open(os.path.join(site_path, 'wp-config-sample.php'), 'r')
    wp_config_str = wp_config_sample.read()
    wp_config_sample.close()
    wp_config_str = wp_config_str.replace("database_name_here", db_name)\
        .replace('username_here', db_user_name)\
        .replace('password_here', db_user_password)\
        .replace('localhost', db_host)
    wp_config = open(site_path+'/wp-config.php', 'w')
    wp_config.write(wp_config_str)
    wp_config.close()

def load_dump(dump_path, site_url):
    dump = open(dump_path, "r")
    dump_str = dump.read().decode('utf-8').replace('__siteurl', site_url)
    return dump_str
'''

def connect_base(host, user, password, base_name=''):
    try:
        return mdb.connect(host, user, password, base_name, charset='utf8')
    except mdb.Error, e:
        print "Error %d: %s" % (e.args[0], e.args[1])


def create_db(db_cursor, db_name):
    try:
        print 'Create db %s' % db_name
        create_db_sql = 'CREATE DATABASE IF NOT EXISTS '+db_name
        db_cursor.execute(create_db_sql)
        db_connect = connect_base('localhost', 'root', 'Oipcom17', db_name)
        return db_connect.cursor()
    except:
        print 'Db exists'
        return False


def load_dump(db_cursor, theme, site_url):
    dump_path = os.path.join(settings.WP_THEMES_PATH, theme, 'dump.sql')
    dump = open(dump_path, "r")
    dump_str = dump.read().decode('utf-8')
    dump_str = dump_str.replace('__siteurl', site_url)
    db_cursor.execute(dump_str)


def url_ok(url):
    r = requests.head(url)
    return r.status_code == 200


def build(theme, spam):
    site_new_theme_path = os.path.join(settings.SITES_DIR, spam.screen_name)
    site_path = os.path.join(site_new_theme_path, theme)
    if not os.path.exists(site_path):
        unzip_wp(site_new_theme_path)
        os.rename(os.path.join(site_new_theme_path, 'wordpress'), site_path)
    themes_path = os.path.join(site_path, 'wp-content/themes')
    if not os.path.exists(os.path.join(themes_path, theme)):
        unzip_theme(theme, themes_path)
    db_name = "wp_%s_%s" % (spam.screen_name, theme)
    db_user_name = 'root'
    db_user_password = 'Oipcom17'
    db_host = 'localhost'
    if not os.path.exists(site_path+'/wp-config.php'):
        create_wp_config(site_path, db_name, db_user_name,
                                       db_user_password, db_host)
    db_cursor = connect_base(db_host, db_user_name, db_user_password).cursor()
    create_db(db_cursor, db_name)
    site_url = os.path.join(settings.DOMAIN_NAME, 'sites', spam.screen_name, theme)
    db_cursor = connect_base(db_host, db_user_name, db_user_password, db_name).cursor()
    load_dump(db_cursor, theme, site_url)
    counter = 0
    while not url_ok(site_url):
        time.sleep(10)
        if counter > 20:
            break
        counter += 1
    client = Client(site_url+'/xmlrpc.php', 'admin', 'password')
    process = getattr(adapters, theme)
    db_cursor = connect_base(db_host, db_user_name, db_user_password, db_name).cursor()
    process(client, db_cursor, spam)
    return site_url
'''