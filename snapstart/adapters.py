import urllib
import os
from wordpress_xmlrpc.compat import xmlrpc_client
from wordpress_xmlrpc.methods import media, posts
from wordpress_xmlrpc.methods.posts import NewPost
from wordpress_xmlrpc import Client, WordPressPost
from wordpress_xmlrpc.methods import options


def change_option_sql(name, value):
    return "UPDATE wp_options SET option_value ='%s' WHERE  option_name = '%s'" % (value, name)


def default(client, db_cursor, spam, count=0):
    db_cursor.execute(change_option_sql('blogname', spam.name))
    #db_cursor.execute(change_option_sql('blogdescription', spam.description))


def aware(client, db_cursor, spam, count=0):
    default(client, db_cursor, spam, count)
    db_cursor.execute(change_option_sql('blogdescription', spam.description[:20]))
    numerals = ['first', 'second', 'third', 'fourth',
                'fifth', 'sixth', 'seventh', 'eighth',
                'ninth', 'tenth', 'eleventh', 'twelfth']
    if len(client.call(posts.GetPosts())) == 0:
        albums = PhotoAlbum.objects.filter(spam__pk=spam.pk)
        for album in albums:
            post = WordPressPost()
            post.title = album.title
            post.content = album.description
            post.post_type = 'portfolio'
            post.post_status = 'publish'
            post.custom_fields = []
            photos = Photo.objects.filter(photo_album__pk=album.pk)
            if count<len(photos):
                count = len(photos)
            if count>len(photos):
                count = len(photos)
            for i in range(0, count):
                photo = photos[i]
                head, tail = os.path.split(photo.src)
                data = {'name': tail, 'type': 'image/jpeg', 'bits': xmlrpc_client.Binary(
                    urllib.urlopen(photo.src).read())}
                response = client.call(media.UploadFile(data))
                if i == 0:
                    post.thumbnail = response['id']
                else:
                    post.custom_fields.append({
                        'key': 'portfolio_%s-slide_thumbnail_id' % numerals[i],
                        'value': response['id']
                    })
            post.id = client.call(NewPost(post))
