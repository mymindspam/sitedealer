# wp = wp_files+db
# create user
# create db
# copy files, make config

import multiprocessing as mp
import os
import re
import urllib2
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "sitedealer.settings")

import django
django.setup()
from django.db import connection
from django.db.models import Q
from deals.models import Site
from snapstart import utils
import sys
URL_RE = re.compile(r'^https?://')

SITES_DIR = sys.argv[1]
WP_ZIP_PATH = sys.argv[2]
WP_THEMES_PATH = sys.argv[3]
WP_DUMPS_PATH = sys.argv[4]

SUPER_USER = 'wp_root'
SUPER_USER_PASSWORD = '1qtg7m99sl11zx'


def make_site(site):
    if site.status == site.NONE:
        site.status = site.BUILDING
        site.save()
        full_site_path = os.path.join(SITES_DIR, site.path)
        if not os.path.exists(full_site_path):
            utils.unzip(WP_ZIP_PATH, full_site_path)
        site_themes_path = os.path.join(full_site_path, 'wp-content', 'themes')
        theme_zip_name = site.theme.path.split('/').pop()
        print theme_zip_name
        if URL_RE.search(site.theme.path):
            urllib2.urlopen(site.theme.path)
            theme_zip_path = theme_zip_name
        else:
            theme_zip_path = os.path.join(WP_THEMES_PATH, site.theme.path)
        print theme_zip_path
        utils.unzip(theme_zip_path, site_themes_path)
        db_name = 'wp_' + site.spam.source_id + site.theme.name
        db_user_name = site.spam.source_id
        db_user_password = site.spam.source_id
        cursor = connection.cursor()
        create_user_sql = '''
        IF NOT EXISTS (SELECT * FROM DBO.SYSUSERS WHERE NAME = %s )
        BEGIN
            CREATE USER %s WITH PASSWORD = "%s"
        END
        ''' % (db_user_name, db_user_name, db_user_password)
        cursor.execute(create_user_sql)
        cursor.execute('CREATE DATABASE IF NOT EXISTS %s' % db_name)
        dump_path = os.path.join(WP_DUMPS_PATH, site.theme.dump_path)
        print dump_path
        utils.load_dump(dump_path, site.url)
        if not os.path.exists(full_site_path+'\wp-config.php'):
            utils.create_wp_config(full_site_path, db_name, db_user_name, db_user_password)
    elif site.status == site.EMPTY:
        pass


def spawn():
    sites = Site.objects.filter(~Q(status=Site.ONLINE) | ~Q(status=Site.DISABLED))
    pool = mp.Pool()  # use all available CPUs
    pool.map(make_site, sites)

if __name__ == "__main__":
    mp.freeze_support()  # optional if the program is not frozen
    spawn()

'''
TODO:
class WPSite(object):
    def __init__(self, full_site_path, ):
'''