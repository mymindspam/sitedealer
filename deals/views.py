#!/usr/bin/env python
# -*- coding: utf-8 -*-
import re
from django.core.exceptions import ObjectDoesNotExist
from django.shortcuts import render, redirect
from loginoauth.vkapi import VkApi
from loginoauth.vkapi import VkApiExeption
from models import Spam
from models import Album
from models import Image
from models import SiteTheme
from models import Site
from models import Client
from django.http import JsonResponse
from django.http import HttpResponse
import hashlib
import os
from sitedealer.settings import SECRET_KEY

PAGE_ITEMS_AMOUNT = 30


def search(request, city_id="0", query_str='', page_id=0):
    args = {}
    vkapi = VkApi(request.session['vk-oauth2_access_token'])
    if not request.is_ajax():
        args['themes'] = SiteTheme.objects.all()
        cities = vkapi.call('database.getCities',
                            country_id=1)
        args['cities'] = cities['items']
        for item in args['cities']:
            if str(item['id']) == city_id:
                args['city_str'] = '%s(%s)' % (item['title'], city_id)
        args['query_str'] = query_str
        args['page_id'] = page_id
    else:
        type = 'group'
        offset = PAGE_ITEMS_AMOUNT
        country = 1
        city = request.GET.get('city')
        query = request.GET.get('query')
        page = int(request.GET.get('page'))
        args['total'], groups = vkapi.serch_and_get_groups(query, type, country, city, offset*page, PAGE_ITEMS_AMOUNT)
        args['groups'] = groups
        return JsonResponse(args)
    return render(request, 'search.html', args)


def get(request, group_id):
    vkapi = VkApi(request.session['vk-oauth2_access_token'])
    return JsonResponse(vkapi.get_full_group_info(group_id))


def new(request):
    try:
        spam = Spam.objects.get(source_id=request.POST.get('group_id'))
    except ObjectDoesNotExist:
        vkapi = VkApi(request.session['vk-oauth2_access_token'])
        spam = Spam(source_id=request.POST.get('group_id'),
                    title=request.POST.get('name'), description=request.POST.get('description'))
        spam.save()
        albums = vkapi.get_albums_and_photos(spam.source_id, request.POST.get('albums'), 3)
        for album_info in albums:
            album = Album(spam=spam, title=album_info['title'], description=album_info['description'])
            album.save()
            sizes = ['photo_1280', 'photo_807', 'photo_604', 'photo_130', 'photo_75']
            for photo_info in album_info['photos']:
                for size in sizes:
                    try:
                        url = photo_info[size]
                    except KeyError:
                        pass
                image = Image(album=album, description=photo_info['text'], url=url)
                image.save()
    theme = SiteTheme.objects.all()[0]
    try:
        site = Site.objects.get(spam=spam, theme=theme)
    except ObjectDoesNotExist:
        md5 = hashlib.md5(SECRET_KEY+str(Site.objects.count())).hexdigest()
        site = Site(spam=spam, theme=theme, url=md5, path=os.path.join(spam.source_id, theme.name))
        site.save()
    for client_id in request.POST.get('contacts').split(','):
        client = Client(id=client_id, site=site)
        client.save()
    return HttpResponse('ok')