#!/usr/bin/python
# -*- coding: utf-8 -*-

from django.db import models


class Spam(models.Model):
    source_id = models.CharField(max_length=140, primary_key=True)
    title = models.CharField(max_length=200)
    description = models.CharField(max_length=400)

    def __unicode__(self):
        return '%s (%s)' % (self.title, self.source_id)


class SiteTheme(models.Model):
    name = models.CharField(max_length=140, primary_key=True)
    path = models.CharField(max_length=300)
    dump_path = models.CharField(max_length=300)

    def __unicode__(self):
        return self.name


class Site(models.Model):
    NONE = 'NN'
    ONLINE = 'ON'
    DISABLED = 'DS'
    BUILDING = 'BL'
    EMPTY = 'EM'
    STATUS = (
        (NONE, 'None'),
        (ONLINE, 'Online'),
        (DISABLED, 'Disabled'),
        (BUILDING, 'Building'),
        (EMPTY, 'Empty'),
    )
    spam = models.ForeignKey(Spam)
    theme = models.ForeignKey(SiteTheme)
    url = models.CharField(max_length=140)
    path = models.CharField(max_length=140)
    status = models.CharField(max_length=2, default=NONE)

    def __unicode__(self):
        return '%s - %s' % (self.spam, self.theme)


class Album(models.Model):
    spam = models.ForeignKey(Spam)
    title = models.CharField(max_length=140)
    description = models.CharField(max_length=140)

    def __unicode__(self):
        return '%s (%s)' % (self.title, self.spam)


class Image(models.Model):
    album = models.ForeignKey(Album)
    description = models.CharField(max_length=140)
    url = models.CharField(max_length=200)

    def __unicode__(self):
        return '%s - %s' % (self.description, self.album)


class Client(models.Model):
    id = models.CharField(max_length=140, primary_key=True)
    site = models.ForeignKey(Site)

    def __unicode__(self):
        return '%s - %s' % (self.id, self.site)
