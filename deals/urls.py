from django.conf.urls import patterns, url
from deals import views
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^/$', views.get, name='new'),
    url(r'^search/$', views.search, name='search'),
    url(r'^search/(?P<city_id>\w+)/(?P<query_str>\w+)/(?P<page_id>\d+)', views.search, name='search'),
    url(r'^get/(?P<group_id>\w+)', views.get, name='get'),
    url(r'^new/', views.new, name='new'),
)

