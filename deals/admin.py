from django.contrib import admin
from deals.models import Album
from deals.models import Spam
from deals.models import Site
from deals.models import Client
from deals.models import SiteTheme


class AlbumInline(admin.StackedInline):
    model = Album


class SpamAdmin(admin.ModelAdmin):
    inlines = [AlbumInline]


class ClientInline(admin.StackedInline):
    model = Client


class SiteAdmin(admin.ModelAdmin):
    inlines = [ClientInline]

admin.site.register(SiteTheme)
admin.site.register(Site, SiteAdmin)
admin.site.register(Spam, SpamAdmin)