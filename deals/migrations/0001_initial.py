# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=140)),
                ('description', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.CharField(max_length=140, serialize=False, primary_key=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Image',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.CharField(max_length=140)),
                ('url', models.CharField(max_length=200)),
                ('album', models.ForeignKey(to='deals.Album')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Site',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('url', models.CharField(max_length=140)),
                ('path', models.CharField(max_length=140)),
                ('online', models.BooleanField(default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='SiteTheme',
            fields=[
                ('name', models.CharField(max_length=140, serialize=False, primary_key=True)),
                ('path', models.CharField(max_length=140)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Spam',
            fields=[
                ('source_id', models.CharField(max_length=140, serialize=False, primary_key=True)),
                ('title', models.CharField(max_length=200)),
                ('description', models.CharField(max_length=400)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='site',
            name='spam',
            field=models.ForeignKey(to='deals.Spam'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='site',
            name='theme',
            field=models.ForeignKey(to='deals.SiteTheme'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='client',
            name='site',
            field=models.ForeignKey(to='deals.Site'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='album',
            name='spam',
            field=models.ForeignKey(to='deals.Spam'),
            preserve_default=True,
        ),
    ]
