# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('deals', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='site',
            name='online',
        ),
        migrations.AddField(
            model_name='site',
            name='status',
            field=models.CharField(default=b'NN', max_length=2),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='sitetheme',
            name='path',
            field=models.CharField(max_length=300),
        ),
    ]
